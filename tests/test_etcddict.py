from common import etcd, get_client
import pytest
import time
import etcd3
import distributed_haproxy_manager
import json
from threading import Thread, Event

class TestType:
    def __init__(self, attr1):
        self.attr1 = attr1

    def to_json(self):
        return json.dumps({'attr1': self.attr1})

    @staticmethod
    def from_json(data):
        return TestType(json.loads(data)['attr1'])

    def __eq__(self, other):
        return self.attr1 == other.attr1

@pytest.fixture
def edict(etcd):
    edict = distributed_haproxy_manager.etcd_dict.EtcdDict(
        TestType, "/edict_test/")
    return edict, etcd

@pytest.mark.timeout(30)
def test_minimal_flow(edict):
    data = TestType('value')
    edict, etcd = edict

    client = get_client(etcd)
    edict_thread = Thread(target=edict.work, args=[client])
    edict_thread.start()
    client.put('/edict_test/element0', data.to_json())
    time.sleep(1)
    edict.stop()
    edict_thread.join()
    assert 'element0' in edict.elements_by_id
    assert data == edict.elements_by_id['element0']
    
@pytest.mark.timeout(30)
def test_resume_connection(edict):
    data1 = TestType('value1')
    data2 = TestType('value2')

    edict, etcd = edict

    client = get_client(etcd)
    edict_thread = Thread(target=edict.work, args=[client])
    edict_thread.start()
    client.put('/edict_test/element0', data1.to_json())
    edict.stop()
    edict_thread.join()

    client.delete_range('/edict_test/element0')
    client.put('/edict_test/element1', data2.to_json())

    client = get_client(etcd)
    edict_thread = Thread(target=edict.work, args=[client])
    edict_thread.start()
    edict.stop()
    edict_thread.join()

    assert 'element0' not in edict.elements_by_id
    assert 'element1' in edict.elements_by_id
    assert data2 == edict.elements_by_id['element1']

@pytest.mark.timeout(30)
def test_stop_event(edict):
    edict, etcd = edict
    client = get_client(etcd)
    stop_event = Event()
    edict_thread = Thread(target=edict.work, args=[client, True, stop_event])
    edict_thread.start()
    stop_event.set()
    time.sleep(6)
    assert not edict_thread.isAlive()
    
