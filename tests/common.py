import pytest
import etcd3
import time
from pytest_docker_tools import build, container, fetch

#etcd_image = fetch(repository='quay.io/coreos/etcd:v3.3.11')

etcd = container(
    image='quay.io/coreos/etcd:v3.3.11',
    environment={
        'ETCDCTL_API': '3',
    },
    command=['etcd',
             '--advertise-client-urls=http://0.0.0.0:2379',
             '--listen-client-urls=http://0.0.0.0:2379'
             ],
    ports={'2379/tcp': None}
)

def get_client(container):
    while (not "ready to serve client requests" in container.logs()):
        time.sleep(1)
    return etcd3.Client(host=container.ips.primary)
