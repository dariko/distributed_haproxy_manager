dataclasses-json
docker
git+https://github.com/dariko/etcd3-py@lock_api#egg=etcd3-py
jinja2
pytest
pytest_docker_tools
pytest_timeout
