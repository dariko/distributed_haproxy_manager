FROM centos:7

ARG http_proxy=""

RUN [[ "$http_proxy" != "" ]] &&  echo "proxy  = $http_proxy" >> /etc/yum.conf || true
    
RUN export https_proxy=$http_proxy \
    && yum install -y gcc make openssl-devel bzip2-devel libffi-devel git \
    && cd /usr/src/ \
    && curl https://www.python.org/ftp/python/3.7.2/Python-3.7.2.tgz | tar xzv \
    && cd Python-3.7.2 \
    && ./configure --enable-optimizations \
    && make -j4 install \
    && ln -s /usr/local/bin/pip3 /usr/local/bin/pip \
    && pip install --upgrade pip \
    && yum clean all \
    && rm -rf /var/cache/yum 
    
COPY requirements.txt /.docker_build_git_requirements
RUN  https_proxy=$http_proxy pip install --no-cache-dir -r /.docker_build_git_requirements

WORKDIR /distributed_haproxy_manager/
COPY . /distributed_haproxy_manager/

RUN pip install /distributed_haproxy_manager
