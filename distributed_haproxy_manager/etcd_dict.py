from threading import Lock, Event
import etcd3
import logging
from requests.exceptions import ConnectionError as req_connectionerror
from urllib3.exceptions import MaxRetryError as urllib_maxretryerror

LOG = logging.getLogger(__name__)


class EtcdDict():
    def __init__(self, _type, prefix, on_update=None, on_fail=None):
        self._type = _type
        self.prefix = prefix
        self.on_update = on_update
        self.on_fail = on_fail

        self.elements_by_id = {}
        self.elements_lock = Lock()
        self.e_quit = Event()

        self.raft_index = None

    def index_from_key(self, key):
        return key.replace(self.prefix, '').split('/')[0]

    def handle_event(self, e):
        LOG.debug('got event %s' % e)
        data = e.value
        key = e.key.decode('utf8')
        if e.type == etcd3.models.EventEventType.PUT:
            try:
                element = self._type.from_json(data)
                element_id = self.index_from_key(key)
            except Exception as ex:
                LOG.error("error parsing %s from string %s: %s" %
                          (self._type.__class__, data, ex))
                return
            if element_id in self.elements_by_id \
                    and element == self.elements_by_id[element_id]:
                LOG.debug('element %s unchanged' % element_id)
                return
            else:
                with self.elements_lock:
                    self.elements_by_id[element_id] = element
                LOG.info('added %s %s: %s' %
                         (self._type, element_id, element))
                if self.on_update:
                    self.on_update()
        elif e.type == etcd3.models.EventEventType.DELETE:
            try:
                element_id = self.index_from_key(key)
            except Exception as ex:
                LOG.error('Error parsing id from key %s: %s' % (key, ex))
            if element_id in self.elements_by_id:
                element = self.elements_by_id[element_id]
                with self.elements_lock:
                    del self.elements_by_id[element_id]
                LOG.info('removec %s %s: %s' %
                         (self._type, element_id, element))
                if self.on_update:
                    self.on_update()
            else:
                LOG.warn("Can't delete missing element %s" % key)

    def refresh(self, client):
        data = client.range(self.prefix, prefix=True)
        elements = {}
        if data.kvs:
            for kv in data.kvs:
                idx = self.index_from_key(kv.key.decode('utf8'))
                elements[idx]=self._type.from_json(kv.value)
        with self.elements_lock:
            self.elements_by_id = elements

    def work(self, client, refresh=True, stop_event=None):
        watcher = None
        try:
            if refresh or (not self.current_revision):
                prefix = self.prefix
                self.raft_index = client.status().raftIndex
                self.refresh(client)
            LOG.info("refreshed %s elements from %s, starting watch" %
                     (len(self.elements_by_id), self.prefix))
            watcher = client.Watcher(key=prefix, prefix=True,
                                     start_revision=self.raft_index)
            watcher.onEvent(self.handle_event)
            watcher.runDaemon()

            while not stop_event.wait(timeout=5):
                client.status()
        except (req_connectionerror, urllib_maxretryerror) as e:
            LOG.error('connection error on %s loop: %s' %
                      (self.__class__, e))
        except Exception:
            LOG.exception('error in watch loop')
        finally:
            if watcher:
                watcher.stop()
            if self.on_fail:
                self.on_fail()

    def stop(self):
        self.e_quit.set()
