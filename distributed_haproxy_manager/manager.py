from threading import Thread, Lock, Event
from .types import Service, AgentData, Assignment, Backend
from .etcd_dict import EtcdDict
from random import choice
from time import sleep
import etcd3
import random
import logging

log = logging.getLogger(__name__)


class Manager():
    def __init__(self, etcd_addresses=None, prefix="/dhm/", lock_ttl=10):
        self.prefix = prefix
        self.etcd_addresses = etcd_addresses
        self.master_lock_ttl = lock_ttl

        self.services = None
        self.agents = None
        self.backends = None
        self.assignments = None
        self.leader_election = None

        self.e_quit = Event()
        self.reconfigure_lock = Lock()
        self.need_refresh = False

        self.e_wakeup = Event()

    def set_refresh(self):
        self.need_refresh = True

    def reconfigure(self, event_object=None):
        services = self.services.elements_by_id.copy()
        agents = self.agents.elements_by_id.copy()
        assignments = self.assignments.elements_by_id.copy()
        new_assignments = set()
        untouched_assignments = set()
        log.debug('assignments: %s' % assignments)
        for service in services.values():
            log.debug('reconfiguring service %s' % service)
            # here it's possible to limit the agent allowed locations
            allowed_agents = [v.name for k, v in agents.items()]
            if not allowed_agents:
                log.error('no agents available to assign service %s' %
                          service)
                continue
            log.debug('allowed agents: %s' % allowed_agents)
            expected_config = service.render_config([])
            found = [[k, v] for k, v in assignments.items()
                     if expected_config == assignments[k].config]
            if found and found[0][1].agent in allowed_agents:
                untouched_assignments.add(found[0][0])
            else:
                agent_name = random.choice(allowed_agents)
                new_assignments.add(Assignment(
                    service.address,
                    service.port,
                    expected_config,
                    agent_name
                    ))
        obsolete_assignments = (set(assignments.keys())
                                - untouched_assignments)
        log.info('obsolete assignments: %s' % obsolete_assignments)
        log.info('assignments: %s' % assignments)
        log.info('obsolete assignments: %s' %
                 ([assignments[x] for x in obsolete_assignments]))
        log.info('new assignments: %s' % new_assignments)
        for x in obsolete_assignments:
            log.debug('remove assignment %s' % assignments[x])
            self.assignments.remove(x)
        for x in new_assignments:
            log.debug('add assignment %s' % x)

    def main_loop_iteration(self, client):
        e_refresh = Event()

        def on_mapped_update():
            self.e_wakeup.set()
            e_refresh.set()

        self.agents = EtcdDict(AgentData, "/dhm/agents/",
                               on_update=on_mapped_update,
                               on_fail=self.e_wakeup.set)
        self.assignments = EtcdDict(Assignment, "/dhm/assignments/",
                                    on_update=on_mapped_update,
                                    on_fail=self.e_wakeup.set)
        self.backends = EtcdDict(Backend, "/dhm/backends/",
                                 on_update=on_mapped_update,
                                 on_fail=self.e_wakeup.set)
        self.services = EtcdDict(Service, "/dhm/services/",
                                 on_update=on_mapped_update,
                                 on_fail=self.e_wakeup.set)
        etcddicts = [self.agents, self.assignments,
                     self.backends, self.services]
        threads = []
        try:
            client.status()
            for etcddict in etcddicts:
                thread = Thread(target=etcddict.work,
                                name='%s etcddict thread' % etcddict.prefix,
                                args=[client])
                threads.append(thread)
            for thread in threads:
                thread.start()

            self.reconfigure()
            while not self.e_quit.is_set():
                if self.e_wakeup.wait(timeout=5):
                    self.e_wakeup.clear()
                    must_restart = False
                    for thread in threads:
                        if not thread.is_alive():
                            log.warn('%s thread dead, will restart')
                            must_restart = True
                    if must_restart:
                        log.warn('main loop must restart')
                        break
                    if e_refresh.is_set():
                        log.info('reconfiguring')
                        self.need_refresh = False
                        self.reconfigure()
        except Exception:
            log.exception('error in manager loop iteration')
        finally:
            try:
                client.close()
            except Exception:
                log.info("can't kill client, shouldn't be a problem")
            for etcddict in etcddicts:
                etcddict.stop()
            for thread in threads:
                thread.join()

    def run(self):
        while not self.e_quit.is_set():
            try:
                address = choice(self.etcd_addresses)
                log.info('connecting to %s' % address)
                client = etcd3.Client(address)
                manager_lock = client.Lock("manager_lock",
                                           self.master_lock_ttl)
                client.status()
                if not manager_lock.is_acquired:
                    log.info('waiting for manager lock')
                with manager_lock:
                    log.debug(manager_lock)
                    log.debug(dir(manager_lock))
                    log.info('obtained manager lock')
                    self.main_loop_iteration(client)
            except Exception:
                log.exception('error in manager main loop')
            sleep(1)

    def stop(self):
        self.e_quit.set()
        self.e_wakeup.set()
