from threading import Thread
from distributed_haproxy_manager import Manager, Agent
from argparse import ArgumentParser
from time import sleep
import socket
import logging
import signal

log = logging.getLogger(__name__)
signal.signal(signal.SIGINT, signal.default_int_handler)
signal.signal(signal.SIGTERM, signal.default_int_handler)

common_options = ArgumentParser()
common_options.add_argument('-a', '--address', action='append',
                            help="etcd addresses (can be repeated)")
common_options.add_argument('-d', '--debug', action='store_true',
                            help='enable debug', default=False)
common_options.add_argument('--prefix', help='prefix', default="/dhm/")
common_options.add_argument('-t', '--ttl', type=int,
                            help='leader lock ttl', default=10)

def manager(args=None):
    parser = common_options
    parser.description = 'Distributed HAproxy manager'
    parser.add_argument('node_id', help='node id')

    args = parser.parse_args()
    if args.address == []:
        args.address = ['127.0.0.1']
    loglevel = logging.INFO
    if args.debug:
        loglevel = logging.DEBUG
    logging.basicConfig(level=loglevel)

    try:
        manager = Manager(args.address, args.prefix, args.ttl)
        manager.run()
    except KeyboardInterrupt:
        manager.stop()
        logging.info('Stopping manager')



def agent(args=None):
    parser = common_options
    parser.description = 'Distributed HAproxy agent'
    parser.add_argument('--datacenter',  help='datacenter', default="dc")
    parser.add_argument('--name', help='name', default=socket.getfqdn())
    parser.add_argument('--restart', help='prefix', action='store_true')

    args = parser.parse_args()
    if args.address == []:
        args.address = ['127.0.0.1']
    loglevel = logging.INFO
    if args.debug:
        loglevel = logging.DEBUG
    logging.basicConfig(level=loglevel)

    while True:
        try:
            agent = Agent(args.address, args.prefix,
                          args.ttl, args.name, args.datacenter)
            agent_thread = Thread(target=agent.run)
            agent_thread.start()
            agent_thread.join()
            logging.error('Agent dead, restarting')
        except KeyboardInterrupt:
            logging.error('ctrlc')
            logging.info('Stopping agent')
            agent.stop()
            agent_thread.join()
            break
        except Exception:
            log.exception('error in main loop')
        finally:
            sleep(1)
