from threading import Thread, Event
from distributed_haproxy_manager.types import Assignment, AgentData
from .etcd_dict import EtcdDict
from random import choice
from time import sleep
import etcd3
import logging

log = logging.getLogger(__name__)


class Agent():
    def __init__(self, etcd_addresses, prefix, announce_ttl,
                 name, datacenter):
        self.prefix = prefix
        self.name = name
        self.datacenter = datacenter
        self.etcd_addresses = etcd_addresses
        self.ttl = announce_ttl

        self.assignments = None

        self.e_wakeup = Event()
        self.e_quit = Event()

    def assignment_callback(self, pool):
        log.info('assignments changed: %s' %
                 self.assignments.elements_by_id)

    def set_refresh(self):
        self.need_refresh = True

    def reconfigure(self):
        log.error('notimplemented')

    def run(self):
        while not self.e_quit.is_set():
            try:
                assignments_thread = None
                address = choice(self.etcd_addresses)
                log.info('connecting to %s' % address)
                client = etcd3.Client(address)
                client.status()
                lease_key_data = AgentData(
                    self.name, self.datacenter).to_json()
                lease_key = ("%sagents/%s-%s" %
                             (self.prefix, self.name, self.datacenter))
                with client.Lease(ttl=self.ttl) as lease:
                    client.put(lease_key, lease_key_data, lease=lease.ID)
                    self.assignments = EtcdDict(
                        Assignment, "/dhm/assignments/",
                        on_update=self.assignment_callback,
                        on_fail=self.e_wakeup.set)
                    assignments_thread = Thread(
                        target=self.assignments.work,
                        args=[client])
                    assignments_thread.start()
                    while not self.e_quit.is_set():
                        if self.e_wakeup.wait(timeout=2):
                            self.e_wakeup.clear()
                            log.info('assignments: %s' %
                                     self.assignments.elements_by_id)
            except Exception:
                self.e_wakeup.set()
                log.exception('error in manager main loop')
            finally:
                if self.assignments:
                    self.assignments.stop()
                if assignments_thread:
                   assignments_thread.join()
            sleep(1)

    def stop(self):
        self.e_quit.set()
