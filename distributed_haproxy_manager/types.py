from dataclasses import dataclass
from dataclasses_json import dataclass_json
from jinja2 import Template
from ipaddress import IPv4Address
import json
import logging

log = logging.getLogger(__name__)


class WeightedEndpoint:
    def __init__(self, address, port, weight=100):
        self.address = str(IPv4Address(address))
        self.port = int(port)
        self.weight = int(weight)

    @staticmethod
    def from_s(string):
        parts = string.split(':')
        if len(parts) > 3:
            raise ValueError("Can't parse WeightedEndpoint from %s" %
                             string)
        weight = 100
        if len(parts) > 2:
            weight = int(parts[2])
        return WeightedEndpoint(parts[0], parts[1], parts[weight])

    @staticmethod
    def from_json(text):
        parsed = json.loads(text)
        return WeightedEndpoint(parsed['address'],
                                parsed['port'],
                                parsed['weight'])

    def to_json(self):
        return json.dumps({'address': self.address,
                           'port': self.port,
                           'weight': self.weight})


@dataclass_json
@dataclass
class Service():
    address: str
    port: str
    config_template: str

    def render_config(self, backends):
        return Template(self.config_template).render({
            'service': self,
            'backends': backends,
        })


@dataclass_json
@dataclass
class AgentData():
    name: str
    datacenter: str


@dataclass_json
@dataclass(frozen=True)
class Assignment():
    address: str
    port: str
    config: str
    agent: str


@dataclass_json
@dataclass
class Backend():
    address: str
    port: str
    service: str
