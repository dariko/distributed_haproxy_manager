#!/usr/bin/env python

from setuptools import setup

setup(name='distributed_haproxy_manager',
      version='0.1.0',
      packages=['distributed_haproxy_manager'],
      entry_points={
          'console_scripts': [
              'dhm-manager = distributed_haproxy_manager.command_line:manager',
              'dhm-agent = distributed_haproxy_manager.command_line:agent',
          ]},
      install_requires=[
          'dataclasses-json',
          'docker',
          'etcd3-py',
          'jinja2',
      ]
      )
